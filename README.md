This repository is intended as storage for small non-related hacks or utilities
that don't deserve their own place. It probably makes no sense to clone it.